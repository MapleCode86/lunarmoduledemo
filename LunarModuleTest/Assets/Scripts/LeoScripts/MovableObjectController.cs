﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LeoScript.MiniGames
{

    public class MovableObjectController : MonoBehaviour
    {
        [SerializeField] private FixedJoystick joystick;

        [SerializeField] private Animator movableObjectController;


        private float moveSpeed = 1.0f;

        private float rotateSpeed = 50.0f;

        private float currentSpeed = 0.0f;
        private const float MAX_SPEED = 0.048f;

        // Update is called once per frame
        void Update()
        {          
            Vector3 movement = Vector3.zero;

            if (!movableObjectController.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                movement = CalculateMovement();               
                ProcessMovement(movement);         
                UpdateAnimator();
            }
        }

        private Vector3 CalculateMovement()
        {
            if (joystick.Direction.magnitude > 0.3f)
            {
                Vector2 joystickDirection = joystick.Direction;

                Vector3 joystickDirectionOnCam = Camera.main.transform.TransformVector(new Vector3(joystickDirection.x, 0.0f, joystickDirection.y));

                Vector3 directionOnPlane = new Vector3(joystickDirectionOnCam.x, 0.0f, joystickDirectionOnCam.z);

                Debug.DrawRay(Camera.main.transform.position, directionOnPlane * 100.0f, Color.yellow);

                Debug.DrawRay(transform.position, directionOnPlane * 100.0f, Color.blue);

                return directionOnPlane;
            }

            return Vector3.zero;
        }

        private void ProcessMovement(Vector3 movement)
        {
            if (movement != Vector3.zero)
            {
                Quaternion rotationThisFrame = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(movement), rotateSpeed * Time.deltaTime);

                transform.rotation = rotationThisFrame;

                currentSpeed = movement.magnitude * moveSpeed * Time.deltaTime;

                currentSpeed = Mathf.Clamp(currentSpeed, 0.0f, MAX_SPEED);

                transform.Translate(transform.forward * currentSpeed, Space.World);
            }
            else
            {
                currentSpeed = 0.0f;
            }
        }
        

        private void UpdateAnimator()
        {
            if (currentSpeed != 0.0f)
            {
                float animatorBlendSpeed = currentSpeed / MAX_SPEED;

                //movableObjectController.SetFloat("Is", animatorBlendSpeed);
                movableObjectController.SetBool("IsMoving", true);
            }
            else
            {
                //movableObjectController.SetFloat("WalkingSpeed", currentSpeed);
                movableObjectController.SetBool("IsMoving", false);

            }

        }

        private void ResetAnimatorParameters()
        {
            currentSpeed = 0.0f;
        }

        public void OnAttackButtonClicked()
        {
            if (movableObjectController.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                return;
            }

            Debug.Log("start attacking");
            movableObjectController.SetBool("IsAttacking", true);
        }

        public void AnimationAttackStart()
        {
          //  attackTrigger.enabled = true;
        }

        public void AttackFinished()
        {
            Debug.Log("stop attacking");
         //   attackTrigger.enabled = false;
            movableObjectController.SetBool("IsAttacking", false);
        }

        public void StopAllAnimation()
        {
            currentSpeed = 0.0f;
            movableObjectController.SetFloat("WalkingSpeed", currentSpeed);
        }


    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LeoScript
{

    public class SelectMenu : MonoBehaviour
    {
        public void OnMarsRoverButtonClicked()
        {
            SceneManager.LoadScene("MarsRover");
        }

        public void OnLunarLanderButtonClicked()
        {
            SceneManager.LoadScene("LunarModule");
        }

        public void OnBackToMenuButtonClicked()
        {
            SceneManager.LoadScene("SelectScene");
        }
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LeoScript
{

    public class LandingArea : MonoBehaviour
    {
        private const string lunarModuleName = "LunarModule";
        public bool IsInLandingArea { get; private set; }

        public void ResetLandingArea()
        {
            IsInLandingArea = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("trigger enter " + other.name);

            if (other.name.Equals(lunarModuleName)) 
            {
                IsInLandingArea = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            Debug.Log("trigger exit " + other.name);

            if (other.name.Equals(lunarModuleName))
            {
                IsInLandingArea = false;
            }
        }
    }
}
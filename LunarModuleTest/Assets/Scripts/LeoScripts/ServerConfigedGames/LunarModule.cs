﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LeoScript
{

    public class LunarModule : MonoBehaviour 
    {
        [SerializeField] private Rigidbody myRigidBody;


        private const string landingGroundName = "LandingGround";
        private const string landingAreaName = "LandingArea";     

        public bool IsLanded { get; private set; }
        public bool IsOnGround { get; private set; }

        public event OnLunarLandedEvent OnLunarLandedEventHandler;
        public delegate void OnLunarLandedEvent();

        Vector3 initPosition = Vector3.zero;

        [SerializeField] private AudioSource audioSource;

        [SerializeField] private AudioClip startSound;
        [SerializeField] private AudioClip landingSound;
        [SerializeField] private AudioClip inflightClip;
        [SerializeField] private AudioClip successClip;

        [SerializeField] private LayerMask layerMask;

        private Queue<AudioClip> audioClipQueue = new Queue<AudioClip>();

        private bool landingSoundPlayed = false;


        public void ResetLunarModule()
        {
            transform.localPosition = initPosition;
            transform.rotation = Quaternion.identity;
            IsLanded = false;
            IsOnGround = false;
            landingSoundPlayed = false;
            audioClipQueue.Clear();
            audioSource.Stop();
            StopCoroutine(CheckAudioQueue());

            StopMoving();
        }

        public void StopMoving()
        {
            myRigidBody.isKinematic = true;
        }

        private void Awake()
        {
            initPosition = transform.localPosition;
        }

        // Use this for initialization
        void Start() 
        {
            LeoJoyStickHelper.Instance.OnRotatingDirectionChangedEventhandler += JoyStickBehaviour;
            myRigidBody = GetComponent<Rigidbody>();
            IsLanded = false;
        }

        public void StartGame()
        {
            myRigidBody.isKinematic = false;
            audioSource.PlayOneShot(startSound);
            audioClipQueue.Enqueue(inflightClip);

            StartCoroutine(CheckAudioQueue());
        }

        IEnumerator CheckAudioQueue()
        {
            while (true)
            { 
                if (!audioSource.isPlaying)
                { 
                    if (audioClipQueue.Count > 0)
                    {
                        AudioClip ac = audioClipQueue.Dequeue();
                        audioSource.PlayOneShot(ac);
                        //yield return new WaitForSeconds(ac.length);

                    }
                }

                yield return new WaitForSeconds(1.0f);
            }
        }

        private void OnDestroy()
        {
            LeoJoyStickHelper.Instance.OnRotatingDirectionChangedEventhandler -= JoyStickBehaviour;
            StopAllCoroutines();
        }


        // Update is called once per frame
        void Update() 
        {
            //apply gravity
            if (LunarModuleGame.IsGameStarted)
            {
                myRigidBody.AddForce(Physics.gravity * 0.01f * LunarModuleGame.Instance.gravitySlider.value);
                myRigidBody.AddTorque(Vector3.up * LunarModuleGame.Instance.spinSlider.value, ForceMode.Acceleration);
                myRigidBody.AddRelativeForce(Vector3.forward * LunarModuleGame.Instance.pinHorizontalForceSlider.value * 
                    LunarModuleGame.Instance.spinSlider.value * 0.05f
                    , ForceMode.Acceleration );
            }



            if (!landingSoundPlayed)
            {
                Debug.DrawRay(transform.position, Vector3.down * 4.0f, Color.red);

                RaycastHit hitInfo;
                if (Physics.Raycast(transform.position, Vector3.down, out hitInfo, 4.0f ,layerMask )) 
                {
                    Debug.Log(hitInfo.collider.name);
                    if (hitInfo.collider.name.Equals(landingGroundName))
                    {
                        Debug.Log("close to ground");
                        landingSoundPlayed = true;

                        if (audioSource.isPlaying)
                        {
                            audioClipQueue.Clear();
                            audioSource.Stop();
                        }

                        audioSource.PlayOneShot(landingSound);
                    }
                }
            }
        }

        //  is called once per frame
        public void JoyStickBehaviour( Vector2 currentDirection , float deltaAngle)
        {
            if (IsLanded)
            {
                return;
            }

            if (LunarModuleGame.IsGameStarted)
            {
                Vector3 forceFromInput = GetInputDirectionOnHoriziontalPlane(currentDirection);
                myRigidBody.AddForce(forceFromInput * 0.01f * LunarModuleGame.Instance.accelSlider.value, ForceMode.VelocityChange);

                
            }
        }

        private Vector3 GetInputDirectionOnHoriziontalPlane(Vector2 joystickDirection)
        {
            Vector3 joystickDirectionOnCam = Camera.main.transform.TransformVector(new Vector3(joystickDirection.x, joystickDirection.y ,0.0f));

            //Vector3 joystickDirectionOnCam = new Vector3(joystickDirection.x, joystickDirection.y,0.0f);

            Vector3 forwardOnHorizontalPlane = Vector3.ProjectOnPlane(Camera.main.transform.forward, Vector3.up);

            //Vector3 dir = Vector3.ProjectOnPlane(joystickDirectionOnCam,forwardOnHorizontalPlane);

            Debug.DrawRay(Camera.main.transform.position, forwardOnHorizontalPlane * 10.0f, Color.red);

            Debug.DrawLine(Camera.main.transform.position, Camera.main.transform.position + joystickDirectionOnCam, Color.yellow,5.0f);

            Debug.DrawLine(transform.position, transform.position + joystickDirectionOnCam, Color.green, 5.0f);

            //Debug.DrawRay(transform.position, dir * 10.0f ,Color.blue);

            return joystickDirectionOnCam;
        }

  

        public void OnCollisionEnter(Collision collision)
        {
            Debug.Log("collision enter " + collision.transform.name);

            if (collision.transform.name.Equals(landingGroundName) || collision.transform.name.Equals("Emulator Ground Plane"))
            {
                IsOnGround = true;

                Invoke("LunarLanded", 0.5f);
            }
        }   

        public void OnCollisionExit(Collision collision)
        {
            Debug.Log("collision exit " + collision.transform.name);

            if (collision.transform.name.Equals(landingGroundName) || collision.transform.name.Equals("Emulator Ground Plane"))
            {
                IsOnGround = false;

                CancelInvoke("LunarLanded");
            }
        }

        public void LunarLanded()
        {
            IsLanded = true;
            Debug.Log("Lunar landed " + IsLanded);
            if (OnLunarLandedEventHandler != null)
            {
                OnLunarLandedEventHandler();
            }
        }  

        public void EnqueueSuccessAudio()
        {
            audioClipQueue.Enqueue(successClip);
        }        
    }
}
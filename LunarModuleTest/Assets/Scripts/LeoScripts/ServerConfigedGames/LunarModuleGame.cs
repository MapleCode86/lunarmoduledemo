﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace LeoScript
{

    public class LunarModuleGame : MonoBehaviour
    {
        [SerializeField] private LunarModule lunarModule;
        [SerializeField] private LandingArea landingArea;

        [SerializeField] private Timer timer;
        [SerializeField] private Text timerText;

        [SerializeField] private GameObject startGamePanel;
        [SerializeField] private GameObject successedPanel;
        [SerializeField] private GameObject failedPanel;


        [SerializeField] private Countdown countdown;


        public Slider gravitySlider;
        public Slider accelSlider;
        public Slider spinSlider;
        public Slider pinHorizontalForceSlider;

        private static bool isGameStarted = false;
        public static bool IsGameStarted
        { 
            get
            {
                return isGameStarted;
            }
        }

        private static LunarModuleGame instance;
        public static LunarModuleGame Instance
        { 
            get
            {
                return instance;
            }
        }

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            QualitySettings.shadowDistance = 40;
            lunarModule.OnLunarLandedEventHandler += LunarModule_OnLunarLandedEventHandler;
            timer.TimeUpEventHandler += Timer_TimeUpEventHandler;
        }

        private void OnDestroy()
        {
            lunarModule.OnLunarLandedEventHandler -= LunarModule_OnLunarLandedEventHandler;
            timer.TimeUpEventHandler -= Timer_TimeUpEventHandler;
        }


        void LunarModule_OnLunarLandedEventHandler()
        { 
            if (IsLandedInTargetArea())
            {
                GameOver(true);
            }
            else
            {
                GameOver(false);
            }
        }

        private bool IsLandedInTargetArea()
        {
            float distance = Vector3.Distance(lunarModule.transform.position, landingArea.transform.position);
            Debug.Log(distance);
            Debug.Log("is in landing area " + landingArea.IsInLandingArea);
            return landingArea.IsInLandingArea;
        }

        void Timer_TimeUpEventHandler()
        {
            timerText.text = "00.00";
            GameOver(false);
        }

        private void LateUpdate()
        {
            if (isGameStarted )
            {
                timerText.text = timer.TimeLeft.ToString("00.00");
            }
        }

        public void OnStartButtonClicked()
        {
            ResetGame();
            countdown.StartCountDownFrom(3, StartGame);
        }

        private void StartGame()
        {
            isGameStarted = true;           
            lunarModule.StartGame();   
            timer.StartTimer(25.0f);
        }

        private void GameOver(bool success)
        {
            isGameStarted = false;
            lunarModule.StopMoving();

            if (success)
            {    
                timer.Stop();
                successedPanel.SetActive(true);
                lunarModule.EnqueueSuccessAudio();
            }
            else
            {
                timer.Stop();

                failedPanel.SetActive(true);
            }
        }

        private void ResetGame()
        {
            lunarModule.ResetLunarModule();
            landingArea.ResetLandingArea();
        }

        
    }
}
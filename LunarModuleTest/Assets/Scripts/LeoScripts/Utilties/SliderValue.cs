﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LeoScript
{
    public class SliderValue : MonoBehaviour
    {
        [SerializeField] private Text text;

        public void OnSliderValuChanged(float value)
        {
            text.text = value.ToString("00.00");
        }
    }
}
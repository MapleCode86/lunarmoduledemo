﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LeoScript
{
    public class LeoJoyStickHelper : MonoBehaviour
    {
        [SerializeField] private FixedJoystick fixedJoystick;

        private float threashold = 3.0f;


        private static LeoJoyStickHelper instance;
        public static LeoJoyStickHelper Instance
        { 
            get
            {
                return instance;
            }
        }

        void Awake()
        {
            instance = this;
        }


        public event OnRotatingDirectionChanged OnRotatingDirectionChangedEventhandler;
        public delegate void OnRotatingDirectionChanged(Vector2 currentDirection, float speed);



        private void FixedUpdate()
        {
            //Debug.Log(fixedJoystick.Direction);

            if (fixedJoystick.IsPointerDown)
            {

                if (OnRotatingDirectionChangedEventhandler != null)
                {
                    //Debug.Log("static hold time " + holdTime);
                    OnRotatingDirectionChangedEventhandler(fixedJoystick.Direction, 0.0f);
                    //OnRotatingDirectionChangedEventhandler(ERotatingDirection.STATIC, fixedJoystick.Direction, deltaAngle);

                }
        

            }
                
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace LeoScript
{

    public class Timer : MonoBehaviour
    {
        public event TimeUpEvent TimeUpEventHandler;
        public delegate void TimeUpEvent();

        private float timePassed = 0.0f;

        private float timeLimit = 0.0f;

        private bool isRunning = false;

        public float TimeLeft
        {
            get
            {
                return timeLimit - timePassed; ;
            }
        }

        public void StartTimer(float countDown)
        {
            this.timeLimit = countDown;
            timePassed = 0.0f;
            isRunning = true;
        }

        public void Stop()
        {
            isRunning = false;
        }

        private void Update()
        {
            if (isRunning)
            {
                timePassed += Time.deltaTime;

                if (timePassed >= timeLimit)
                {
                    isRunning = false;
                    timePassed = 0;
                    timeLimit = 0;

                    if (TimeUpEventHandler != null)
                    {
                        TimeUpEventHandler();
                    }
                }
            }
        }


    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


namespace LeoScript
{

    public class Countdown : MonoBehaviour
    {
        [SerializeField] private GameObject canvas;
        [SerializeField] private Text countdownLabel;

        private bool isRunning = false;

        public void StartCountDownFrom(int seconds, System.Action Callback)
        {
            if (isRunning)
            {
                Debug.LogError("duplicate countdown");
                return;
            }

            canvas.SetActive(true);
            StartCoroutine(ShowCountDown(seconds, Callback));
        }

        private IEnumerator ShowCountDown(int seconds, Action Callback)
        {
            StopCoroutine(TextAnimation());

            while (true)
            {
                countdownLabel.text = seconds.ToString();

                StartCoroutine(TextAnimation());

                if (seconds <= 0)
                {
                    break;
                }

                seconds--;

                yield return new WaitForSeconds(1.0f);
            }

            if (Callback != null)
            {
                Callback();
            }

            canvas.SetActive(false);
            isRunning = false;
        }

        private IEnumerator TextAnimation()
        {
            //countdownLabel.fontSize = 150;
            float duration = 1.0f;

            Vector3 scale = Vector3.one * 0.75f;

            while (duration >= 0.0f)
            {
                duration -= Time.deltaTime;

                scale += Vector3.one * Time.deltaTime * 4;

                if (scale.x >= 1.0f)
                {
                    scale = Vector3.one;
                }

                countdownLabel.transform.localScale = scale;

                countdownLabel.color = new Color(1, 1, 1, duration);

                yield return Time.deltaTime;
            }
        }
    }
}